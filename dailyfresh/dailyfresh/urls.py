# coding:utf8
"""dailyfresh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.views.static import serve
from order.views import CommentView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tinymce/', include('tinymce.urls')), # 富文本编辑器
    url(r'^search', include('haystack.urls')), # 全文检索框架
    url(r'^user/', include('users.urls', namespace='user')), # 用户模块
    url(r'^cart/', include('cart.urls', namespace='cart')), # 购物车模块
    url(r'^order/', include('order.urls', namespace='order')), # 订单模块
    url(r'^', include('goods.urls', namespace='goods')), # 商品模块
    url(r'^static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^static/media/banner/(?P<path>.*)$', serve, {'document_root': 'static/media/banner/'}),
    url(r'^static/media/type/(?P<path>.*)$', serve, {'document_root': 'static/media/type/'}),
    url(r'^goods/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^user/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^list/1/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^list/2/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^list/3/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^list/4/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^list/5/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^list/6/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^cart/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^order/comment/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^order/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    url(r'^user/order/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
    # url(r'^order/comment/?order_id=(?P<order_id>.+)$', CommentView.as_view(), name='comment'),
    # url(r'^order/comment/static/media/goods/(?P<path>.*)$', serve, {'document_root': 'static/media/goods/'}),
]
